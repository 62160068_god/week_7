/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wongsathorn.usermanagement;

import java.util.ArrayList;

/**
 *
 * @author iUser
 */
public class UserService {
    private static ArrayList<User> userList = null;
    static{
        userList = new ArrayList<>();
        //Mock Data
        userList.add(new User("admin","password"));
        userList.add(new User("admin1","password"));
        userList.add(new User("admin2","password"));
    }
    public static boolean addUser(User user){
        userList.add(user);
        return true;
    }
    public static boolean delUser(User user){
        userList.remove(user);
        return true;
    }
    public static boolean addUser(int index){
        userList.remove(index);
        return true;
    }
   public static ArrayList<User> getUser(){
       return userList;
   } 
   public static User getUser(int index){
       return userList.get(index);
   }
   public static boolean updateUser(int index,User user){
       userList.set(index,user);
       return true;
   }
   public static void save(){
       
   }
    public static void load(){
       
   }

    public static boolean delUser(int index) {
        userList.remove(index);
        save();
        return true;
        
    }
}
